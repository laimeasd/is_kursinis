import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipTestModule } from '../../../test.module';
import { OrderReceiptDetailComponent } from 'app/entities/order-receipt/order-receipt-detail.component';
import { OrderReceipt } from 'app/shared/model/order-receipt.model';

describe('Component Tests', () => {
  describe('OrderReceipt Management Detail Component', () => {
    let comp: OrderReceiptDetailComponent;
    let fixture: ComponentFixture<OrderReceiptDetailComponent>;
    const route = ({ data: of({ orderReceipt: new OrderReceipt(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipTestModule],
        declarations: [OrderReceiptDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(OrderReceiptDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrderReceiptDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load orderReceipt on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.orderReceipt).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
