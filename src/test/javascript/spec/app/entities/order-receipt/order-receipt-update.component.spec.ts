import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JhipTestModule } from '../../../test.module';
import { OrderReceiptUpdateComponent } from 'app/entities/order-receipt/order-receipt-update.component';
import { OrderReceiptService } from 'app/entities/order-receipt/order-receipt.service';
import { OrderReceipt } from 'app/shared/model/order-receipt.model';

describe('Component Tests', () => {
  describe('OrderReceipt Management Update Component', () => {
    let comp: OrderReceiptUpdateComponent;
    let fixture: ComponentFixture<OrderReceiptUpdateComponent>;
    let service: OrderReceiptService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipTestModule],
        declarations: [OrderReceiptUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(OrderReceiptUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrderReceiptUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrderReceiptService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrderReceipt(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrderReceipt();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
