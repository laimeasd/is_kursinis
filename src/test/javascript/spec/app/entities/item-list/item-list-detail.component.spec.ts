import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipTestModule } from '../../../test.module';
import { ItemListDetailComponent } from 'app/entities/item-list/item-list-detail.component';
import { ItemList } from 'app/shared/model/item-list.model';

describe('Component Tests', () => {
  describe('ItemList Management Detail Component', () => {
    let comp: ItemListDetailComponent;
    let fixture: ComponentFixture<ItemListDetailComponent>;
    const route = ({ data: of({ itemList: new ItemList(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipTestModule],
        declarations: [ItemListDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ItemListDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ItemListDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load itemList on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.itemList).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
