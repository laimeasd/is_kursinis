import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JhipTestModule } from '../../../test.module';
import { ItemListUpdateComponent } from 'app/entities/item-list/item-list-update.component';
import { ItemListService } from 'app/entities/item-list/item-list.service';
import { ItemList } from 'app/shared/model/item-list.model';

describe('Component Tests', () => {
  describe('ItemList Management Update Component', () => {
    let comp: ItemListUpdateComponent;
    let fixture: ComponentFixture<ItemListUpdateComponent>;
    let service: ItemListService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipTestModule],
        declarations: [ItemListUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ItemListUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ItemListUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ItemListService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ItemList(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ItemList();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
