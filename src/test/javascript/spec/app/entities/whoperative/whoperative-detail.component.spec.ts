import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipTestModule } from '../../../test.module';
import { WhoperativeDetailComponent } from 'app/entities/whoperative/whoperative-detail.component';
import { Whoperative } from 'app/shared/model/whoperative.model';

describe('Component Tests', () => {
  describe('Whoperative Management Detail Component', () => {
    let comp: WhoperativeDetailComponent;
    let fixture: ComponentFixture<WhoperativeDetailComponent>;
    const route = ({ data: of({ whoperative: new Whoperative(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipTestModule],
        declarations: [WhoperativeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(WhoperativeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WhoperativeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load whoperative on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.whoperative).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
