import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipTestModule } from '../../../test.module';
import { WhoperativeComponent } from 'app/entities/whoperative/whoperative.component';
import { WhoperativeService } from 'app/entities/whoperative/whoperative.service';
import { Whoperative } from 'app/shared/model/whoperative.model';

describe('Component Tests', () => {
  describe('Whoperative Management Component', () => {
    let comp: WhoperativeComponent;
    let fixture: ComponentFixture<WhoperativeComponent>;
    let service: WhoperativeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipTestModule],
        declarations: [WhoperativeComponent],
      })
        .overrideTemplate(WhoperativeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WhoperativeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WhoperativeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Whoperative(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.whoperatives && comp.whoperatives[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
