import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JhipTestModule } from '../../../test.module';
import { WhoperativeUpdateComponent } from 'app/entities/whoperative/whoperative-update.component';
import { WhoperativeService } from 'app/entities/whoperative/whoperative.service';
import { Whoperative } from 'app/shared/model/whoperative.model';

describe('Component Tests', () => {
  describe('Whoperative Management Update Component', () => {
    let comp: WhoperativeUpdateComponent;
    let fixture: ComponentFixture<WhoperativeUpdateComponent>;
    let service: WhoperativeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipTestModule],
        declarations: [WhoperativeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(WhoperativeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WhoperativeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WhoperativeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Whoperative(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Whoperative();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
