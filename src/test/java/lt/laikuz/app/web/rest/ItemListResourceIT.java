package lt.laikuz.app.web.rest;

import lt.laikuz.app.JhipApp;
import lt.laikuz.app.domain.ItemList;
import lt.laikuz.app.repository.ItemListRepository;
import lt.laikuz.app.service.ItemListService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import lt.laikuz.app.domain.enumeration.ListPurpose;
/**
 * Integration tests for the {@link ItemListResource} REST controller.
 */
@SpringBootTest(classes = JhipApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ItemListResourceIT {

    private static final Integer DEFAULT_ITEM_COUNT = 1;
    private static final Integer UPDATED_ITEM_COUNT = 2;

    private static final ListPurpose DEFAULT_LIST_PURPOSE = ListPurpose.ACCEPT;
    private static final ListPurpose UPDATED_LIST_PURPOSE = ListPurpose.RETURN;

    @Autowired
    private ItemListRepository itemListRepository;

    @Autowired
    private ItemListService itemListService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restItemListMockMvc;

    private ItemList itemList;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ItemList createEntity(EntityManager em) {
        ItemList itemList = new ItemList()
            .itemCount(DEFAULT_ITEM_COUNT)
            .listPurpose(DEFAULT_LIST_PURPOSE);
        return itemList;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ItemList createUpdatedEntity(EntityManager em) {
        ItemList itemList = new ItemList()
            .itemCount(UPDATED_ITEM_COUNT)
            .listPurpose(UPDATED_LIST_PURPOSE);
        return itemList;
    }

    @BeforeEach
    public void initTest() {
        itemList = createEntity(em);
    }

    @Test
    @Transactional
    public void createItemList() throws Exception {
        int databaseSizeBeforeCreate = itemListRepository.findAll().size();
        // Create the ItemList
        restItemListMockMvc.perform(post("/api/item-lists")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(itemList)))
            .andExpect(status().isCreated());

        // Validate the ItemList in the database
        List<ItemList> itemListList = itemListRepository.findAll();
        assertThat(itemListList).hasSize(databaseSizeBeforeCreate + 1);
        ItemList testItemList = itemListList.get(itemListList.size() - 1);
        assertThat(testItemList.getItemCount()).isEqualTo(DEFAULT_ITEM_COUNT);
        assertThat(testItemList.getListPurpose()).isEqualTo(DEFAULT_LIST_PURPOSE);
    }

    @Test
    @Transactional
    public void createItemListWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = itemListRepository.findAll().size();

        // Create the ItemList with an existing ID
        itemList.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restItemListMockMvc.perform(post("/api/item-lists")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(itemList)))
            .andExpect(status().isBadRequest());

        // Validate the ItemList in the database
        List<ItemList> itemListList = itemListRepository.findAll();
        assertThat(itemListList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllItemLists() throws Exception {
        // Initialize the database
        itemListRepository.saveAndFlush(itemList);

        // Get all the itemListList
        restItemListMockMvc.perform(get("/api/item-lists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(itemList.getId().intValue())))
            .andExpect(jsonPath("$.[*].itemCount").value(hasItem(DEFAULT_ITEM_COUNT)))
            .andExpect(jsonPath("$.[*].listPurpose").value(hasItem(DEFAULT_LIST_PURPOSE.toString())));
    }
    
    @Test
    @Transactional
    public void getItemList() throws Exception {
        // Initialize the database
        itemListRepository.saveAndFlush(itemList);

        // Get the itemList
        restItemListMockMvc.perform(get("/api/item-lists/{id}", itemList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(itemList.getId().intValue()))
            .andExpect(jsonPath("$.itemCount").value(DEFAULT_ITEM_COUNT))
            .andExpect(jsonPath("$.listPurpose").value(DEFAULT_LIST_PURPOSE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingItemList() throws Exception {
        // Get the itemList
        restItemListMockMvc.perform(get("/api/item-lists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateItemList() throws Exception {
        // Initialize the database
        itemListService.save(itemList);

        int databaseSizeBeforeUpdate = itemListRepository.findAll().size();

        // Update the itemList
        ItemList updatedItemList = itemListRepository.findById(itemList.getId()).get();
        // Disconnect from session so that the updates on updatedItemList are not directly saved in db
        em.detach(updatedItemList);
        updatedItemList
            .itemCount(UPDATED_ITEM_COUNT)
            .listPurpose(UPDATED_LIST_PURPOSE);

        restItemListMockMvc.perform(put("/api/item-lists")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedItemList)))
            .andExpect(status().isOk());

        // Validate the ItemList in the database
        List<ItemList> itemListList = itemListRepository.findAll();
        assertThat(itemListList).hasSize(databaseSizeBeforeUpdate);
        ItemList testItemList = itemListList.get(itemListList.size() - 1);
        assertThat(testItemList.getItemCount()).isEqualTo(UPDATED_ITEM_COUNT);
        assertThat(testItemList.getListPurpose()).isEqualTo(UPDATED_LIST_PURPOSE);
    }

    @Test
    @Transactional
    public void updateNonExistingItemList() throws Exception {
        int databaseSizeBeforeUpdate = itemListRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restItemListMockMvc.perform(put("/api/item-lists")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(itemList)))
            .andExpect(status().isBadRequest());

        // Validate the ItemList in the database
        List<ItemList> itemListList = itemListRepository.findAll();
        assertThat(itemListList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteItemList() throws Exception {
        // Initialize the database
        itemListService.save(itemList);

        int databaseSizeBeforeDelete = itemListRepository.findAll().size();

        // Delete the itemList
        restItemListMockMvc.perform(delete("/api/item-lists/{id}", itemList.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ItemList> itemListList = itemListRepository.findAll();
        assertThat(itemListList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
