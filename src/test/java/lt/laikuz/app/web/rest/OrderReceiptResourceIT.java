package lt.laikuz.app.web.rest;

import lt.laikuz.app.JhipApp;
import lt.laikuz.app.domain.OrderReceipt;
import lt.laikuz.app.repository.OrderReceiptRepository;
import lt.laikuz.app.service.OrderReceiptService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrderReceiptResource} REST controller.
 */
@SpringBootTest(classes = JhipApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OrderReceiptResourceIT {

    private static final Integer DEFAULT_TOTAL_ITEM_COUNT = 1;
    private static final Integer UPDATED_TOTAL_ITEM_COUNT = 2;

    private static final Float DEFAULT_TOTAL_ORDER_WEIGHT = 1F;
    private static final Float UPDATED_TOTAL_ORDER_WEIGHT = 2F;

    private static final Float DEFAULT_TOTAL_ORDER_VOLUME = 1F;
    private static final Float UPDATED_TOTAL_ORDER_VOLUME = 2F;

    @Autowired
    private OrderReceiptRepository orderReceiptRepository;

    @Autowired
    private OrderReceiptService orderReceiptService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderReceiptMockMvc;

    private OrderReceipt orderReceipt;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderReceipt createEntity(EntityManager em) {
        OrderReceipt orderReceipt = new OrderReceipt()
            .totalItemCount(DEFAULT_TOTAL_ITEM_COUNT)
            .totalOrderWeight(DEFAULT_TOTAL_ORDER_WEIGHT)
            .totalOrderVolume(DEFAULT_TOTAL_ORDER_VOLUME);
        return orderReceipt;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderReceipt createUpdatedEntity(EntityManager em) {
        OrderReceipt orderReceipt = new OrderReceipt()
            .totalItemCount(UPDATED_TOTAL_ITEM_COUNT)
            .totalOrderWeight(UPDATED_TOTAL_ORDER_WEIGHT)
            .totalOrderVolume(UPDATED_TOTAL_ORDER_VOLUME);
        return orderReceipt;
    }

    @BeforeEach
    public void initTest() {
        orderReceipt = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderReceipt() throws Exception {
        int databaseSizeBeforeCreate = orderReceiptRepository.findAll().size();
        // Create the OrderReceipt
        restOrderReceiptMockMvc.perform(post("/api/order-receipts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderReceipt)))
            .andExpect(status().isCreated());

        // Validate the OrderReceipt in the database
        List<OrderReceipt> orderReceiptList = orderReceiptRepository.findAll();
        assertThat(orderReceiptList).hasSize(databaseSizeBeforeCreate + 1);
        OrderReceipt testOrderReceipt = orderReceiptList.get(orderReceiptList.size() - 1);
        assertThat(testOrderReceipt.getTotalItemCount()).isEqualTo(DEFAULT_TOTAL_ITEM_COUNT);
        assertThat(testOrderReceipt.getTotalOrderWeight()).isEqualTo(DEFAULT_TOTAL_ORDER_WEIGHT);
        assertThat(testOrderReceipt.getTotalOrderVolume()).isEqualTo(DEFAULT_TOTAL_ORDER_VOLUME);
    }

    @Test
    @Transactional
    public void createOrderReceiptWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderReceiptRepository.findAll().size();

        // Create the OrderReceipt with an existing ID
        orderReceipt.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderReceiptMockMvc.perform(post("/api/order-receipts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderReceipt)))
            .andExpect(status().isBadRequest());

        // Validate the OrderReceipt in the database
        List<OrderReceipt> orderReceiptList = orderReceiptRepository.findAll();
        assertThat(orderReceiptList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrderReceipts() throws Exception {
        // Initialize the database
        orderReceiptRepository.saveAndFlush(orderReceipt);

        // Get all the orderReceiptList
        restOrderReceiptMockMvc.perform(get("/api/order-receipts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderReceipt.getId().intValue())))
            .andExpect(jsonPath("$.[*].totalItemCount").value(hasItem(DEFAULT_TOTAL_ITEM_COUNT)))
            .andExpect(jsonPath("$.[*].totalOrderWeight").value(hasItem(DEFAULT_TOTAL_ORDER_WEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].totalOrderVolume").value(hasItem(DEFAULT_TOTAL_ORDER_VOLUME.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getOrderReceipt() throws Exception {
        // Initialize the database
        orderReceiptRepository.saveAndFlush(orderReceipt);

        // Get the orderReceipt
        restOrderReceiptMockMvc.perform(get("/api/order-receipts/{id}", orderReceipt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderReceipt.getId().intValue()))
            .andExpect(jsonPath("$.totalItemCount").value(DEFAULT_TOTAL_ITEM_COUNT))
            .andExpect(jsonPath("$.totalOrderWeight").value(DEFAULT_TOTAL_ORDER_WEIGHT.doubleValue()))
            .andExpect(jsonPath("$.totalOrderVolume").value(DEFAULT_TOTAL_ORDER_VOLUME.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingOrderReceipt() throws Exception {
        // Get the orderReceipt
        restOrderReceiptMockMvc.perform(get("/api/order-receipts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderReceipt() throws Exception {
        // Initialize the database
        orderReceiptService.save(orderReceipt);

        int databaseSizeBeforeUpdate = orderReceiptRepository.findAll().size();

        // Update the orderReceipt
        OrderReceipt updatedOrderReceipt = orderReceiptRepository.findById(orderReceipt.getId()).get();
        // Disconnect from session so that the updates on updatedOrderReceipt are not directly saved in db
        em.detach(updatedOrderReceipt);
        updatedOrderReceipt
            .totalItemCount(UPDATED_TOTAL_ITEM_COUNT)
            .totalOrderWeight(UPDATED_TOTAL_ORDER_WEIGHT)
            .totalOrderVolume(UPDATED_TOTAL_ORDER_VOLUME);

        restOrderReceiptMockMvc.perform(put("/api/order-receipts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrderReceipt)))
            .andExpect(status().isOk());

        // Validate the OrderReceipt in the database
        List<OrderReceipt> orderReceiptList = orderReceiptRepository.findAll();
        assertThat(orderReceiptList).hasSize(databaseSizeBeforeUpdate);
        OrderReceipt testOrderReceipt = orderReceiptList.get(orderReceiptList.size() - 1);
        assertThat(testOrderReceipt.getTotalItemCount()).isEqualTo(UPDATED_TOTAL_ITEM_COUNT);
        assertThat(testOrderReceipt.getTotalOrderWeight()).isEqualTo(UPDATED_TOTAL_ORDER_WEIGHT);
        assertThat(testOrderReceipt.getTotalOrderVolume()).isEqualTo(UPDATED_TOTAL_ORDER_VOLUME);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderReceipt() throws Exception {
        int databaseSizeBeforeUpdate = orderReceiptRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderReceiptMockMvc.perform(put("/api/order-receipts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderReceipt)))
            .andExpect(status().isBadRequest());

        // Validate the OrderReceipt in the database
        List<OrderReceipt> orderReceiptList = orderReceiptRepository.findAll();
        assertThat(orderReceiptList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrderReceipt() throws Exception {
        // Initialize the database
        orderReceiptService.save(orderReceipt);

        int databaseSizeBeforeDelete = orderReceiptRepository.findAll().size();

        // Delete the orderReceipt
        restOrderReceiptMockMvc.perform(delete("/api/order-receipts/{id}", orderReceipt.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderReceipt> orderReceiptList = orderReceiptRepository.findAll();
        assertThat(orderReceiptList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
