package lt.laikuz.app.web.rest;

import lt.laikuz.app.JhipApp;
import lt.laikuz.app.domain.Whoperative;
import lt.laikuz.app.repository.WhoperativeRepository;
import lt.laikuz.app.service.WhoperativeService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WhoperativeResource} REST controller.
 */
@SpringBootTest(classes = JhipApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class WhoperativeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SURNAME = "AAAAAAAAAA";
    private static final String UPDATED_SURNAME = "BBBBBBBBBB";

    @Autowired
    private WhoperativeRepository whoperativeRepository;

    @Mock
    private WhoperativeRepository whoperativeRepositoryMock;

    @Mock
    private WhoperativeService whoperativeServiceMock;

    @Autowired
    private WhoperativeService whoperativeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWhoperativeMockMvc;

    private Whoperative whoperative;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Whoperative createEntity(EntityManager em) {
        Whoperative whoperative = new Whoperative()
            .name(DEFAULT_NAME)
            .surname(DEFAULT_SURNAME);
        return whoperative;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Whoperative createUpdatedEntity(EntityManager em) {
        Whoperative whoperative = new Whoperative()
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME);
        return whoperative;
    }

    @BeforeEach
    public void initTest() {
        whoperative = createEntity(em);
    }

    @Test
    @Transactional
    public void createWhoperative() throws Exception {
        int databaseSizeBeforeCreate = whoperativeRepository.findAll().size();
        // Create the Whoperative
        restWhoperativeMockMvc.perform(post("/api/whoperatives")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(whoperative)))
            .andExpect(status().isCreated());

        // Validate the Whoperative in the database
        List<Whoperative> whoperativeList = whoperativeRepository.findAll();
        assertThat(whoperativeList).hasSize(databaseSizeBeforeCreate + 1);
        Whoperative testWhoperative = whoperativeList.get(whoperativeList.size() - 1);
        assertThat(testWhoperative.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testWhoperative.getSurname()).isEqualTo(DEFAULT_SURNAME);
    }

    @Test
    @Transactional
    public void createWhoperativeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = whoperativeRepository.findAll().size();

        // Create the Whoperative with an existing ID
        whoperative.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWhoperativeMockMvc.perform(post("/api/whoperatives")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(whoperative)))
            .andExpect(status().isBadRequest());

        // Validate the Whoperative in the database
        List<Whoperative> whoperativeList = whoperativeRepository.findAll();
        assertThat(whoperativeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllWhoperatives() throws Exception {
        // Initialize the database
        whoperativeRepository.saveAndFlush(whoperative);

        // Get all the whoperativeList
        restWhoperativeMockMvc.perform(get("/api/whoperatives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(whoperative.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].surname").value(hasItem(DEFAULT_SURNAME)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllWhoperativesWithEagerRelationshipsIsEnabled() throws Exception {
        when(whoperativeServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restWhoperativeMockMvc.perform(get("/api/whoperatives?eagerload=true"))
            .andExpect(status().isOk());

        verify(whoperativeServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllWhoperativesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(whoperativeServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restWhoperativeMockMvc.perform(get("/api/whoperatives?eagerload=true"))
            .andExpect(status().isOk());

        verify(whoperativeServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getWhoperative() throws Exception {
        // Initialize the database
        whoperativeRepository.saveAndFlush(whoperative);

        // Get the whoperative
        restWhoperativeMockMvc.perform(get("/api/whoperatives/{id}", whoperative.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(whoperative.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.surname").value(DEFAULT_SURNAME));
    }
    @Test
    @Transactional
    public void getNonExistingWhoperative() throws Exception {
        // Get the whoperative
        restWhoperativeMockMvc.perform(get("/api/whoperatives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWhoperative() throws Exception {
        // Initialize the database
        whoperativeService.save(whoperative);

        int databaseSizeBeforeUpdate = whoperativeRepository.findAll().size();

        // Update the whoperative
        Whoperative updatedWhoperative = whoperativeRepository.findById(whoperative.getId()).get();
        // Disconnect from session so that the updates on updatedWhoperative are not directly saved in db
        em.detach(updatedWhoperative);
        updatedWhoperative
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME);

        restWhoperativeMockMvc.perform(put("/api/whoperatives")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedWhoperative)))
            .andExpect(status().isOk());

        // Validate the Whoperative in the database
        List<Whoperative> whoperativeList = whoperativeRepository.findAll();
        assertThat(whoperativeList).hasSize(databaseSizeBeforeUpdate);
        Whoperative testWhoperative = whoperativeList.get(whoperativeList.size() - 1);
        assertThat(testWhoperative.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testWhoperative.getSurname()).isEqualTo(UPDATED_SURNAME);
    }

    @Test
    @Transactional
    public void updateNonExistingWhoperative() throws Exception {
        int databaseSizeBeforeUpdate = whoperativeRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWhoperativeMockMvc.perform(put("/api/whoperatives")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(whoperative)))
            .andExpect(status().isBadRequest());

        // Validate the Whoperative in the database
        List<Whoperative> whoperativeList = whoperativeRepository.findAll();
        assertThat(whoperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWhoperative() throws Exception {
        // Initialize the database
        whoperativeService.save(whoperative);

        int databaseSizeBeforeDelete = whoperativeRepository.findAll().size();

        // Delete the whoperative
        restWhoperativeMockMvc.perform(delete("/api/whoperatives/{id}", whoperative.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Whoperative> whoperativeList = whoperativeRepository.findAll();
        assertThat(whoperativeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
