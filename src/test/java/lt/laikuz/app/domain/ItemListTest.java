package lt.laikuz.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import lt.laikuz.app.web.rest.TestUtil;

public class ItemListTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ItemList.class);
        ItemList itemList1 = new ItemList();
        itemList1.setId(1L);
        ItemList itemList2 = new ItemList();
        itemList2.setId(itemList1.getId());
        assertThat(itemList1).isEqualTo(itemList2);
        itemList2.setId(2L);
        assertThat(itemList1).isNotEqualTo(itemList2);
        itemList1.setId(null);
        assertThat(itemList1).isNotEqualTo(itemList2);
    }
}
