package lt.laikuz.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import lt.laikuz.app.web.rest.TestUtil;

public class OrderReceiptTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderReceipt.class);
        OrderReceipt orderReceipt1 = new OrderReceipt();
        orderReceipt1.setId(1L);
        OrderReceipt orderReceipt2 = new OrderReceipt();
        orderReceipt2.setId(orderReceipt1.getId());
        assertThat(orderReceipt1).isEqualTo(orderReceipt2);
        orderReceipt2.setId(2L);
        assertThat(orderReceipt1).isNotEqualTo(orderReceipt2);
        orderReceipt1.setId(null);
        assertThat(orderReceipt1).isNotEqualTo(orderReceipt2);
    }
}
