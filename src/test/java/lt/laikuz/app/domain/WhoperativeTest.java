package lt.laikuz.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import lt.laikuz.app.web.rest.TestUtil;

public class WhoperativeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Whoperative.class);
        Whoperative whoperative1 = new Whoperative();
        whoperative1.setId(1L);
        Whoperative whoperative2 = new Whoperative();
        whoperative2.setId(whoperative1.getId());
        assertThat(whoperative1).isEqualTo(whoperative2);
        whoperative2.setId(2L);
        assertThat(whoperative1).isNotEqualTo(whoperative2);
        whoperative1.setId(null);
        assertThat(whoperative1).isNotEqualTo(whoperative2);
    }
}
