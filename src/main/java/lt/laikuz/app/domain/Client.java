package lt.laikuz.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "client")
    private Set<OrderReceipt> orderReceipts = new HashSet<>();

    @OneToMany(mappedBy = "client")
    private Set<Item> items = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "clients", allowSetters = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Client name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public Client surname(String surname) {
        this.surname = surname;
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public Client address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<OrderReceipt> getOrderReceipts() {
        return orderReceipts;
    }

    public Client orderReceipts(Set<OrderReceipt> orderReceipts) {
        this.orderReceipts = orderReceipts;
        return this;
    }

    public Client addOrderReceipt(OrderReceipt orderReceipt) {
        this.orderReceipts.add(orderReceipt);
        orderReceipt.setClient(this);
        return this;
    }

    public Client removeOrderReceipt(OrderReceipt orderReceipt) {
        this.orderReceipts.remove(orderReceipt);
        orderReceipt.setClient(null);
        return this;
    }

    public void setOrderReceipts(Set<OrderReceipt> orderReceipts) {
        this.orderReceipts = orderReceipts;
    }

    public Set<Item> getItems() {
        return items;
    }

    public Client items(Set<Item> items) {
        this.items = items;
        return this;
    }

    public Client addItem(Item item) {
        this.items.add(item);
        item.setClient(this);
        return this;
    }

    public Client removeItem(Item item) {
        this.items.remove(item);
        item.setClient(null);
        return this;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public User getUser() {
        return user;
    }

    public Client user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            ", address='" + getAddress() + "'" +
            "}";
    }
}
