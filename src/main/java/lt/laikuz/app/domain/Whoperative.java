package lt.laikuz.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Whoperative.
 */
@Entity
@Table(name = "whoperative")
public class Whoperative implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @OneToMany(mappedBy = "whoperative")
    private Set<Item> items = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "whoperatives", allowSetters = true)
    private User user;

    @ManyToMany
    @JoinTable(name = "whoperative_orderreceipt",
               joinColumns = @JoinColumn(name = "whoperative_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "orderreceipt_id", referencedColumnName = "id"))
    private Set<OrderReceipt> orderreceipts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Whoperative name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public Whoperative surname(String surname) {
        this.surname = surname;
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Set<Item> getItems() {
        return items;
    }

    public Whoperative items(Set<Item> items) {
        this.items = items;
        return this;
    }

    public Whoperative addItem(Item item) {
        this.items.add(item);
        item.setWhoperative(this);
        return this;
    }

    public Whoperative removeItem(Item item) {
        this.items.remove(item);
        item.setWhoperative(null);
        return this;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public User getUser() {
        return user;
    }

    public Whoperative user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<OrderReceipt> getOrderreceipts() {
        return orderreceipts;
    }

    public Whoperative orderreceipts(Set<OrderReceipt> orderReceipts) {
        this.orderreceipts = orderReceipts;
        return this;
    }

    public Whoperative addOrderreceipt(OrderReceipt orderReceipt) {
        this.orderreceipts.add(orderReceipt);
        orderReceipt.getWhoperatives().add(this);
        return this;
    }

    public Whoperative removeOrderreceipt(OrderReceipt orderReceipt) {
        this.orderreceipts.remove(orderReceipt);
        orderReceipt.getWhoperatives().remove(this);
        return this;
    }

    public void setOrderreceipts(Set<OrderReceipt> orderReceipts) {
        this.orderreceipts = orderReceipts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Whoperative)) {
            return false;
        }
        return id != null && id.equals(((Whoperative) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Whoperative{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            "}";
    }
}
