package lt.laikuz.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A OrderReceipt.
 */
@Entity
@Table(name = "order_receipt")
public class OrderReceipt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "total_item_count")
    private Integer totalItemCount;

    @Column(name = "total_order_weight")
    private Float totalOrderWeight;

    @Column(name = "total_order_volume")
    private Float totalOrderVolume;

    @OneToMany(mappedBy = "orderreceipt")
    private Set<ItemList> itemLists = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "orderReceipts", allowSetters = true)
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = "orderReceipts", allowSetters = true)
    private Manager manager;

    @ManyToMany(mappedBy = "orderreceipts")
    @JsonIgnore
    private Set<Whoperative> whoperatives = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalItemCount() {
        return totalItemCount;
    }

    public OrderReceipt totalItemCount(Integer totalItemCount) {
        this.totalItemCount = totalItemCount;
        return this;
    }

    public void setTotalItemCount(Integer totalItemCount) {
        this.totalItemCount = totalItemCount;
    }

    public Float getTotalOrderWeight() {
        return totalOrderWeight;
    }

    public OrderReceipt totalOrderWeight(Float totalOrderWeight) {
        this.totalOrderWeight = totalOrderWeight;
        return this;
    }

    public void setTotalOrderWeight(Float totalOrderWeight) {
        this.totalOrderWeight = totalOrderWeight;
    }

    public Float getTotalOrderVolume() {
        return totalOrderVolume;
    }

    public OrderReceipt totalOrderVolume(Float totalOrderVolume) {
        this.totalOrderVolume = totalOrderVolume;
        return this;
    }

    public void setTotalOrderVolume(Float totalOrderVolume) {
        this.totalOrderVolume = totalOrderVolume;
    }

    public Set<ItemList> getItemLists() {
        return itemLists;
    }

    public OrderReceipt itemLists(Set<ItemList> itemLists) {
        this.itemLists = itemLists;
        return this;
    }

    public OrderReceipt addItemList(ItemList itemList) {
        this.itemLists.add(itemList);
        itemList.setOrderreceipt(this);
        return this;
    }

    public OrderReceipt removeItemList(ItemList itemList) {
        this.itemLists.remove(itemList);
        itemList.setOrderreceipt(null);
        return this;
    }

    public void setItemLists(Set<ItemList> itemLists) {
        this.itemLists = itemLists;
    }

    public Client getClient() {
        return client;
    }

    public OrderReceipt client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Manager getManager() {
        return manager;
    }

    public OrderReceipt manager(Manager manager) {
        this.manager = manager;
        return this;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Set<Whoperative> getWhoperatives() {
        return whoperatives;
    }

    public OrderReceipt whoperatives(Set<Whoperative> whoperatives) {
        this.whoperatives = whoperatives;
        return this;
    }

    public OrderReceipt addWhoperative(Whoperative whoperative) {
        this.whoperatives.add(whoperative);
        whoperative.getOrderreceipts().add(this);
        return this;
    }

    public OrderReceipt removeWhoperative(Whoperative whoperative) {
        this.whoperatives.remove(whoperative);
        whoperative.getOrderreceipts().remove(this);
        return this;
    }

    public void setWhoperatives(Set<Whoperative> whoperatives) {
        this.whoperatives = whoperatives;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderReceipt)) {
            return false;
        }
        return id != null && id.equals(((OrderReceipt) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderReceipt{" +
            "id=" + getId() +
            ", totalItemCount=" + getTotalItemCount() +
            ", totalOrderWeight=" + getTotalOrderWeight() +
            ", totalOrderVolume=" + getTotalOrderVolume() +
            "}";
    }
}
