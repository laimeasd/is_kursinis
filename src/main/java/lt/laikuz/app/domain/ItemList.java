package lt.laikuz.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import lt.laikuz.app.domain.enumeration.ListPurpose;

/**
 * A ItemList.
 */
@Entity
@Table(name = "item_list")
public class ItemList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "item_count")
    private Integer itemCount;

    @Enumerated(EnumType.STRING)
    @Column(name = "list_purpose")
    private ListPurpose listPurpose;

    @OneToMany(mappedBy = "itemlist")
    private Set<Item> items = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "itemLists", allowSetters = true)
    private OrderReceipt orderreceipt;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public ItemList itemCount(Integer itemCount) {
        this.itemCount = itemCount;
        return this;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public ListPurpose getListPurpose() {
        return listPurpose;
    }

    public ItemList listPurpose(ListPurpose listPurpose) {
        this.listPurpose = listPurpose;
        return this;
    }

    public void setListPurpose(ListPurpose listPurpose) {
        this.listPurpose = listPurpose;
    }

    public Set<Item> getItems() {
        return items;
    }

    public ItemList items(Set<Item> items) {
        this.items = items;
        return this;
    }

    public ItemList addItem(Item item) {
        this.items.add(item);
        item.setItemlist(this);
        return this;
    }

    public ItemList removeItem(Item item) {
        this.items.remove(item);
        item.setItemlist(null);
        return this;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public OrderReceipt getOrderreceipt() {
        return orderreceipt;
    }

    public ItemList orderreceipt(OrderReceipt orderReceipt) {
        this.orderreceipt = orderReceipt;
        return this;
    }

    public void setOrderreceipt(OrderReceipt orderReceipt) {
        this.orderreceipt = orderReceipt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ItemList)) {
            return false;
        }
        return id != null && id.equals(((ItemList) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ItemList{" +
            "id=" + getId() +
            ", itemCount=" + getItemCount() +
            ", listPurpose='" + getListPurpose() + "'" +
            "}";
    }
}
