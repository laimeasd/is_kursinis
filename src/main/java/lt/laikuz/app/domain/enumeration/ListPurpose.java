package lt.laikuz.app.domain.enumeration;

/**
 * The ListPurpose enumeration.
 */
public enum ListPurpose {
    ACCEPT, RETURN
}
