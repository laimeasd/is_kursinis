package lt.laikuz.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Manager.
 */
@Entity
@Table(name = "manager")
public class Manager implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "department")
    private String department;

    @OneToMany(mappedBy = "manager")
    private Set<OrderReceipt> orderReceipts = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "managers", allowSetters = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Manager name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public Manager surname(String surname) {
        this.surname = surname;
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDepartment() {
        return department;
    }

    public Manager department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Set<OrderReceipt> getOrderReceipts() {
        return orderReceipts;
    }

    public Manager orderReceipts(Set<OrderReceipt> orderReceipts) {
        this.orderReceipts = orderReceipts;
        return this;
    }

    public Manager addOrderReceipt(OrderReceipt orderReceipt) {
        this.orderReceipts.add(orderReceipt);
        orderReceipt.setManager(this);
        return this;
    }

    public Manager removeOrderReceipt(OrderReceipt orderReceipt) {
        this.orderReceipts.remove(orderReceipt);
        orderReceipt.setManager(null);
        return this;
    }

    public void setOrderReceipts(Set<OrderReceipt> orderReceipts) {
        this.orderReceipts = orderReceipts;
    }

    public User getUser() {
        return user;
    }

    public Manager user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Manager)) {
            return false;
        }
        return id != null && id.equals(((Manager) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Manager{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            ", department='" + getDepartment() + "'" +
            "}";
    }
}
