package lt.laikuz.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Item.
 */
@Entity
@Table(name = "item")
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "item_weight")
    private Float itemWeight;

    @Column(name = "item_volume")
    private Float itemVolume;

    @Column(name = "location")
    private String location;

    @ManyToOne
    @JsonIgnoreProperties(value = "items", allowSetters = true)
    private ItemList itemlist;

    @ManyToOne
    @JsonIgnoreProperties(value = "items", allowSetters = true)
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = "items", allowSetters = true)
    private Whoperative whoperative;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public Item itemName(String itemName) {
        this.itemName = itemName;
        return this;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Float getItemWeight() {
        return itemWeight;
    }

    public Item itemWeight(Float itemWeight) {
        this.itemWeight = itemWeight;
        return this;
    }

    public void setItemWeight(Float itemWeight) {
        this.itemWeight = itemWeight;
    }

    public Float getItemVolume() {
        return itemVolume;
    }

    public Item itemVolume(Float itemVolume) {
        this.itemVolume = itemVolume;
        return this;
    }

    public void setItemVolume(Float itemVolume) {
        this.itemVolume = itemVolume;
    }

    public String getLocation() {
        return location;
    }

    public Item location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ItemList getItemlist() {
        return itemlist;
    }

    public Item itemlist(ItemList itemList) {
        this.itemlist = itemList;
        return this;
    }

    public void setItemlist(ItemList itemList) {
        this.itemlist = itemList;
    }

    public Client getClient() {
        return client;
    }

    public Item client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Whoperative getWhoperative() {
        return whoperative;
    }

    public Item whoperative(Whoperative whoperative) {
        this.whoperative = whoperative;
        return this;
    }

    public void setWhoperative(Whoperative whoperative) {
        this.whoperative = whoperative;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Item)) {
            return false;
        }
        return id != null && id.equals(((Item) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Item{" +
            "id=" + getId() +
            ", itemName='" + getItemName() + "'" +
            ", itemWeight=" + getItemWeight() +
            ", itemVolume=" + getItemVolume() +
            ", location='" + getLocation() + "'" +
            "}";
    }
}
