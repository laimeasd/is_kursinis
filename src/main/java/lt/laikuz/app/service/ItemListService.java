package lt.laikuz.app.service;

import lt.laikuz.app.domain.ItemList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ItemList}.
 */
public interface ItemListService {

    /**
     * Save a itemList.
     *
     * @param itemList the entity to save.
     * @return the persisted entity.
     */
    ItemList save(ItemList itemList);

    /**
     * Get all the itemLists.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ItemList> findAll(Pageable pageable);


    /**
     * Get the "id" itemList.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ItemList> findOne(Long id);

    /**
     * Delete the "id" itemList.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

	Page<ItemList> findByOrderreceiptId(Long id, Pageable pageable);
}
