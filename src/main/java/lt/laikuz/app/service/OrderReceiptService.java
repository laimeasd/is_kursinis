package lt.laikuz.app.service;

import lt.laikuz.app.domain.OrderReceipt;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link OrderReceipt}.
 */
public interface OrderReceiptService {

    /**
     * Save a orderReceipt.
     *
     * @param orderReceipt the entity to save.
     * @return the persisted entity.
     */
    OrderReceipt save(OrderReceipt orderReceipt);

    /**
     * Get all the orderReceipts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrderReceipt> findAll(Pageable pageable);

    Page<OrderReceipt> findByClientUserLogin(String login, Pageable pageable);
    /**
     * Get the "id" orderReceipt.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrderReceipt> findOne(Long id);

    /**
     * Delete the "id" orderReceipt.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

}
