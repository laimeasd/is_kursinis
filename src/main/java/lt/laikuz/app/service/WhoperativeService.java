package lt.laikuz.app.service;

import lt.laikuz.app.domain.Whoperative;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Whoperative}.
 */
public interface WhoperativeService {

    /**
     * Save a whoperative.
     *
     * @param whoperative the entity to save.
     * @return the persisted entity.
     */
    Whoperative save(Whoperative whoperative);

    /**
     * Get all the whoperatives.
     *
     * @return the list of entities.
     */
    List<Whoperative> findAll();

    /**
     * Get all the whoperatives with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<Whoperative> findAllWithEagerRelationships(Pageable pageable);


    /**
     * Get the "id" whoperative.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Whoperative> findOne(Long id);

    /**
     * Delete the "id" whoperative.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
