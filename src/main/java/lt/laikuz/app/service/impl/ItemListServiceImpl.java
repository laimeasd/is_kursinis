package lt.laikuz.app.service.impl;

import lt.laikuz.app.service.ItemListService;
import lt.laikuz.app.domain.ItemList;
import lt.laikuz.app.repository.ItemListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ItemList}.
 */
@Service
@Transactional
public class ItemListServiceImpl implements ItemListService {

    private final Logger log = LoggerFactory.getLogger(ItemListServiceImpl.class);

    private final ItemListRepository itemListRepository;

    public ItemListServiceImpl(ItemListRepository itemListRepository) {
        this.itemListRepository = itemListRepository;
    }

    @Override
    public ItemList save(ItemList itemList) {
        log.debug("Request to save ItemList : {}", itemList);
        return itemListRepository.save(itemList);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ItemList> findAll(Pageable pageable) {
        log.debug("Request to get all ItemLists");
        return itemListRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ItemList> findOne(Long id) {
        log.debug("Request to get ItemList : {}", id);
        return itemListRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ItemList : {}", id);
        itemListRepository.deleteById(id);
    }

	@Override
	public Page<ItemList> findByOrderreceiptId(Long id, Pageable pageable) {
		return itemListRepository.findByOrderreceiptId(id, pageable);
	}
}
