package lt.laikuz.app.service.impl;

import lt.laikuz.app.service.WhoperativeService;
import lt.laikuz.app.domain.Whoperative;
import lt.laikuz.app.repository.WhoperativeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Whoperative}.
 */
@Service
@Transactional
public class WhoperativeServiceImpl implements WhoperativeService {

    private final Logger log = LoggerFactory.getLogger(WhoperativeServiceImpl.class);

    private final WhoperativeRepository whoperativeRepository;

    public WhoperativeServiceImpl(WhoperativeRepository whoperativeRepository) {
        this.whoperativeRepository = whoperativeRepository;
    }

    @Override
    public Whoperative save(Whoperative whoperative) {
        log.debug("Request to save Whoperative : {}", whoperative);
        return whoperativeRepository.save(whoperative);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Whoperative> findAll() {
        log.debug("Request to get all Whoperatives");
        return whoperativeRepository.findAllWithEagerRelationships();
    }


    public Page<Whoperative> findAllWithEagerRelationships(Pageable pageable) {
        return whoperativeRepository.findAllWithEagerRelationships(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Whoperative> findOne(Long id) {
        log.debug("Request to get Whoperative : {}", id);
        return whoperativeRepository.findOneWithEagerRelationships(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Whoperative : {}", id);
        whoperativeRepository.deleteById(id);
    }
}
