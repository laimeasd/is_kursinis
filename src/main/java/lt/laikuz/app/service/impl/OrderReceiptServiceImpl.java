package lt.laikuz.app.service.impl;

import lt.laikuz.app.service.OrderReceiptService;
import lt.laikuz.app.domain.OrderReceipt;
import lt.laikuz.app.repository.OrderReceiptRepository;
import lt.laikuz.app.security.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderReceipt}.
 */
@Service
@Transactional
public class OrderReceiptServiceImpl implements OrderReceiptService {

    private final Logger log = LoggerFactory.getLogger(OrderReceiptServiceImpl.class);

    private final OrderReceiptRepository orderReceiptRepository;

    public OrderReceiptServiceImpl(OrderReceiptRepository orderReceiptRepository) {
        this.orderReceiptRepository = orderReceiptRepository;
    }

    @Override
    public OrderReceipt save(OrderReceipt orderReceipt) {
        log.debug("Request to save OrderReceipt : {}", orderReceipt);
        return orderReceiptRepository.save(orderReceipt);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<OrderReceipt> findAll(Pageable pageable) {
        log.debug("Request to get all OrderReceipts");
        return orderReceiptRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<OrderReceipt> findOne(Long id) {
        log.debug("Request to get OrderReceipt : {}", id);
        return orderReceiptRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderReceipt : {}", id);
        orderReceiptRepository.deleteById(id);
    }

	@Override
	public Page<OrderReceipt> findByClientUserLogin(String login, Pageable pageable) {
		log.debug("Request to get all OrderReceipts");
        return orderReceiptRepository.findByClientUserLogin(login, pageable);
	}
}
