/**
 * View Models used by Spring MVC REST controllers.
 */
package lt.laikuz.app.web.rest.vm;
