package lt.laikuz.app.web.rest;

import lt.laikuz.app.domain.ItemList;
import lt.laikuz.app.service.ItemListService;
import lt.laikuz.app.service.OrderReceiptService;
import lt.laikuz.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link lt.laikuz.app.domain.ItemList}.
 */
@RestController
@RequestMapping("/api")
public class ItemListResource {

    private final Logger log = LoggerFactory.getLogger(ItemListResource.class);

    private static final String ENTITY_NAME = "itemList";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ItemListService itemListService;
    private final OrderReceiptService orderReceiptService;

    public ItemListResource(ItemListService itemListService, OrderReceiptService orderReceiptService) {
        this.itemListService = itemListService;
        this.orderReceiptService = orderReceiptService;
    }

    /**
     * {@code POST  /item-lists} : Create a new itemList.
     *
     * @param itemList the itemList to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new itemList, or with status {@code 400 (Bad Request)} if the itemList has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/item-lists")
    public ResponseEntity<ItemList> createItemList(@RequestBody ItemList itemList) throws URISyntaxException {
        log.debug("REST request to save ItemList : {}", itemList);
        if (itemList.getId() != null) {
            throw new BadRequestAlertException("A new itemList cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ItemList result = itemListService.save(itemList);
        return ResponseEntity.created(new URI("/api/item-lists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    @PostMapping("item-lists/{id}/addForOrder")
    public ResponseEntity<ItemList> createItemListForOrder(@PathVariable Long id, @RequestBody ItemList itemList) throws URISyntaxException{
    	
    	if (itemList.getId() != null) {
            throw new BadRequestAlertException("A new itemList cannot already have an ID", ENTITY_NAME, "idexists");
        }
    	itemList.setOrderreceipt(orderReceiptService.findOne(id).get());
        ItemList result = itemListService.save(itemList);
        return ResponseEntity.created(new URI("/api/item-lists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
		
    	
    }
    /**
     * {@code PUT  /item-lists} : Updates an existing itemList.
     *
     * @param itemList the itemList to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated itemList,
     * or with status {@code 400 (Bad Request)} if the itemList is not valid,
     * or with status {@code 500 (Internal Server Error)} if the itemList couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/item-lists")
    public ResponseEntity<ItemList> updateItemList(@RequestBody ItemList itemList) throws URISyntaxException {
        log.debug("REST request to update ItemList : {}", itemList);
        if (itemList.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ItemList result = itemListService.save(itemList);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, itemList.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /item-lists} : get all the itemLists.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of itemLists in body.
     */
    @GetMapping("/item-lists")
    public ResponseEntity<List<ItemList>> getAllItemLists(Pageable pageable) {
        log.debug("REST request to get a page of ItemLists");
        Page<ItemList> page = itemListService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /item-lists/:id} : get the "id" itemList.
     *
     * @param id the id of the itemList to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the itemList, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/item-lists/{id}")
    public ResponseEntity<ItemList> getItemList(@PathVariable Long id) {
    	log.debug("REST request to get a page of ItemLists");
        Optional<ItemList> itemlist = itemListService.findOne(id);
        return ResponseUtil.wrapOrNotFound(itemlist);
    }

    @GetMapping("item-lists/byorder/{id}")
    public ResponseEntity<List<ItemList>> getItemListByOrder(@PathVariable Long id, Pageable pageable) {
    	log.debug("REST request to get a page of ItemLists");
    	Page<ItemList> page = itemListService.findByOrderreceiptId(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    /**
     * {@code DELETE  /item-lists/:id} : delete the "id" itemList.
     *
     * @param id the id of the itemList to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/item-lists/{id}")
    public ResponseEntity<Void> deleteItemList(@PathVariable Long id) {
        log.debug("REST request to delete ItemList : {}", id);
        itemListService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
