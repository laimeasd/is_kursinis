package lt.laikuz.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lt.laikuz.app.domain.OrderReceipt;
import lt.laikuz.app.security.SecurityUtils;
import lt.laikuz.app.service.ClientService;
import lt.laikuz.app.service.OrderReceiptService;
import lt.laikuz.app.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link lt.laikuz.app.domain.OrderReceipt}.
 */
@RestController
@RequestMapping("/api")
public class OrderReceiptResource {

    private final Logger log = LoggerFactory.getLogger(OrderReceiptResource.class);

    private static final String ENTITY_NAME = "orderReceipt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderReceiptService orderReceiptService;
    private final ClientService clientService;

    public OrderReceiptResource(OrderReceiptService orderReceiptService, ClientService clientService) {
        this.clientService = clientService;
		this.orderReceiptService = orderReceiptService;
    }

    /**
     * {@code POST  /order-receipts} : Create a new orderReceipt.
     *
     * @param orderReceipt the orderReceipt to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderReceipt, or with status {@code 400 (Bad Request)} if the orderReceipt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/order-receipts/asd")
    public ResponseEntity<OrderReceipt> createOrderReceipt(@RequestBody OrderReceipt orderReceipt) throws URISyntaxException {
        log.debug("REST request to save OrderReceipt : {}", orderReceipt);
        if (orderReceipt.getId() != null) {
            throw new BadRequestAlertException("A new orderReceipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        orderReceipt.setClient(clientService.findByUserLogin(SecurityUtils.getCurrentUserLogin().get()));
        OrderReceipt result = orderReceiptService.save(orderReceipt);
        return ResponseEntity.created(new URI("/api/order-receipts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-receipts} : Updates an existing orderReceipt.
     *
     * @param orderReceipt the orderReceipt to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderReceipt,
     * or with status {@code 400 (Bad Request)} if the orderReceipt is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderReceipt couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/order-receipts")
    public ResponseEntity<OrderReceipt> updateOrderReceipt(@RequestBody OrderReceipt orderReceipt) throws URISyntaxException {
        log.debug("REST request to update OrderReceipt : {}", orderReceipt);
        if (orderReceipt.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderReceipt result = orderReceiptService.save(orderReceipt);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orderReceipt.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /order-receipts} : get all the orderReceipts.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderReceipts in body.
     */
    @GetMapping("/order-receipts")
    public ResponseEntity<List<OrderReceipt>> getAllOrderReceipts(Pageable pageable) {
        log.debug("REST request to get a page of OrderReceipts");
        Page<OrderReceipt> page = orderReceiptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    @GetMapping("/order-receipts/myorders")
    public ResponseEntity<List<OrderReceipt>> getAllOrderReceiptsForClient(Pageable pageable) {
        log.debug("REST request to get a page of OrderReceipts");
        Page<OrderReceipt> page = orderReceiptService.findByClientUserLogin(SecurityUtils.getCurrentUserLogin().get(), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    

    /**
     * {@code GET  /order-receipts/:id} : get the "id" orderReceipt.
     *
     * @param id the id of the orderReceipt to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderReceipt, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/order-receipts/{id}")
    public ResponseEntity<OrderReceipt> getOrderReceipt(@PathVariable Long id) {
        log.debug("REST request to get OrderReceipt : {}", id);
        Optional<OrderReceipt> orderReceipt = orderReceiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderReceipt);
    }

    /**
     * {@code DELETE  /order-receipts/:id} : delete the "id" orderReceipt.
     *
     * @param id the id of the orderReceipt to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/order-receipts/{id}")
    public ResponseEntity<Void> deleteOrderReceipt(@PathVariable Long id) {
        log.debug("REST request to delete OrderReceipt : {}", id);
        orderReceiptService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
