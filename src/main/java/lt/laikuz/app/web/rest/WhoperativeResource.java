package lt.laikuz.app.web.rest;

import lt.laikuz.app.domain.Whoperative;
import lt.laikuz.app.service.WhoperativeService;
import lt.laikuz.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link lt.laikuz.app.domain.Whoperative}.
 */
@RestController
@RequestMapping("/api")
public class WhoperativeResource {

    private final Logger log = LoggerFactory.getLogger(WhoperativeResource.class);

    private static final String ENTITY_NAME = "whoperative";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WhoperativeService whoperativeService;

    public WhoperativeResource(WhoperativeService whoperativeService) {
        this.whoperativeService = whoperativeService;
    }

    /**
     * {@code POST  /whoperatives} : Create a new whoperative.
     *
     * @param whoperative the whoperative to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new whoperative, or with status {@code 400 (Bad Request)} if the whoperative has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/whoperatives")
    public ResponseEntity<Whoperative> createWhoperative(@RequestBody Whoperative whoperative) throws URISyntaxException {
        log.debug("REST request to save Whoperative : {}", whoperative);
        if (whoperative.getId() != null) {
            throw new BadRequestAlertException("A new whoperative cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Whoperative result = whoperativeService.save(whoperative);
        return ResponseEntity.created(new URI("/api/whoperatives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /whoperatives} : Updates an existing whoperative.
     *
     * @param whoperative the whoperative to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated whoperative,
     * or with status {@code 400 (Bad Request)} if the whoperative is not valid,
     * or with status {@code 500 (Internal Server Error)} if the whoperative couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/whoperatives")
    public ResponseEntity<Whoperative> updateWhoperative(@RequestBody Whoperative whoperative) throws URISyntaxException {
        log.debug("REST request to update Whoperative : {}", whoperative);
        if (whoperative.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Whoperative result = whoperativeService.save(whoperative);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, whoperative.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /whoperatives} : get all the whoperatives.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of whoperatives in body.
     */
    @GetMapping("/whoperatives")
    public List<Whoperative> getAllWhoperatives(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Whoperatives");
        return whoperativeService.findAll();
    }

    /**
     * {@code GET  /whoperatives/:id} : get the "id" whoperative.
     *
     * @param id the id of the whoperative to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the whoperative, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/whoperatives/{id}")
    public ResponseEntity<Whoperative> getWhoperative(@PathVariable Long id) {
        log.debug("REST request to get Whoperative : {}", id);
        Optional<Whoperative> whoperative = whoperativeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(whoperative);
    }

    /**
     * {@code DELETE  /whoperatives/:id} : delete the "id" whoperative.
     *
     * @param id the id of the whoperative to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/whoperatives/{id}")
    public ResponseEntity<Void> deleteWhoperative(@PathVariable Long id) {
        log.debug("REST request to delete Whoperative : {}", id);
        whoperativeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
