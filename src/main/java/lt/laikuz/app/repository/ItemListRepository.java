package lt.laikuz.app.repository;

import lt.laikuz.app.domain.ItemList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ItemList entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ItemListRepository extends JpaRepository<ItemList, Long> {

	Page<ItemList> findByOrderreceiptId(Long id, Pageable pageable);
	
}
