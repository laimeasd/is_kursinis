package lt.laikuz.app.repository;

import lt.laikuz.app.domain.OrderReceipt;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the OrderReceipt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderReceiptRepository extends JpaRepository<OrderReceipt, Long> {

	Page<OrderReceipt> findByClientUserLogin(String login, Pageable pageable);
}
