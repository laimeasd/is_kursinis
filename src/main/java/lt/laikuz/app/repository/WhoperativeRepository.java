package lt.laikuz.app.repository;

import lt.laikuz.app.domain.Whoperative;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Whoperative entity.
 */
@Repository
public interface WhoperativeRepository extends JpaRepository<Whoperative, Long> {

    @Query("select whoperative from Whoperative whoperative where whoperative.user.login = ?#{principal.username}")
    List<Whoperative> findByUserIsCurrentUser();

    @Query(value = "select distinct whoperative from Whoperative whoperative left join fetch whoperative.orderreceipts",
        countQuery = "select count(distinct whoperative) from Whoperative whoperative")
    Page<Whoperative> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct whoperative from Whoperative whoperative left join fetch whoperative.orderreceipts")
    List<Whoperative> findAllWithEagerRelationships();

    @Query("select whoperative from Whoperative whoperative left join fetch whoperative.orderreceipts where whoperative.id =:id")
    Optional<Whoperative> findOneWithEagerRelationships(@Param("id") Long id);
}
