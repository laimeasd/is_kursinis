package lt.laikuz.app.repository;

import lt.laikuz.app.domain.Manager;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Manager entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {

    @Query("select manager from Manager manager where manager.user.login = ?#{principal.username}")
    List<Manager> findByUserIsCurrentUser();
}
