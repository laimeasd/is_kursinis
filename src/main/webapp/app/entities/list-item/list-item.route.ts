import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { IItem, Item } from 'app/shared/model/item.model';
import { ItemService } from '../item/item.service';
import { ItemComponent } from '../item/item.component';
import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ListItemComponent } from './list-item.component';

@Injectable({ providedIn: 'root' })
export class ListItemResolve implements Resolve<IItem[]> {
  constructor(private service: ItemService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IItem[]> | Observable<never> {
    const id = route.params['id'];

    return this.service.findByList(id).pipe(
      flatMap((items: HttpResponse<Item[]>) => {
        if (items.body) {
          return of(items.body);
        } else {
          this.router.navigate(['404']);
          return EMPTY;
        }
      })
    );
  }
}

export const itemsRoute: Routes = [
  {
    path: 'bylist/:id',
    component: ListItemComponent,
    resolve: {
      items: ListItemResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Items',
    },
    canActivate: [UserRouteAccessService],
  },
];
