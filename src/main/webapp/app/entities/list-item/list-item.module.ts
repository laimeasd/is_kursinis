import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipSharedModule } from 'app/shared/shared.module';
import { ListItemComponent } from './list-item.component';
import { itemsRoute } from './list-item.route';

@NgModule({
  //imports: [JhipSharedModule, RouterModule.forChild(itemListRoute), RouterModule.forChild(orderListRoute)],
  imports: [JhipSharedModule, RouterModule.forChild(itemsRoute)],
  declarations: [ListItemComponent],
})
export class JhipIListItemModule {}
