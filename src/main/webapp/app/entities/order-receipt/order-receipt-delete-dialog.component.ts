import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrderReceipt } from 'app/shared/model/order-receipt.model';
import { OrderReceiptService } from './order-receipt.service';

@Component({
  templateUrl: './order-receipt-delete-dialog.component.html',
})
export class OrderReceiptDeleteDialogComponent {
  orderReceipt?: IOrderReceipt;

  constructor(
    protected orderReceiptService: OrderReceiptService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.orderReceiptService.delete(id).subscribe(() => {
      this.eventManager.broadcast('orderReceiptListModification');
      this.activeModal.close();
    });
  }
}
