import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrderReceipt } from 'app/shared/model/order-receipt.model';

@Component({
  selector: 'jhi-order-receipt-detail',
  templateUrl: './order-receipt-detail.component.html',
})
export class OrderReceiptDetailComponent implements OnInit {
  orderReceipt: IOrderReceipt | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderReceipt }) => (this.orderReceipt = orderReceipt));
  }

  previousState(): void {
    window.history.back();
  }
}
