import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOrderReceipt } from 'app/shared/model/order-receipt.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { OrderReceiptService } from './order-receipt.service';
import { OrderReceiptDeleteDialogComponent } from './order-receipt-delete-dialog.component';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-order-receipt',
  templateUrl: './order-receipt.component.html',
})
export class OrderReceiptComponent implements OnInit, OnDestroy {
  orderReceipts: IOrderReceipt[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected orderReceiptService: OrderReceiptService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService
  ) {
    this.orderReceipts = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    if (this.accountService.hasAnyAuthority('ROLE_MANAGER')) {
      this.orderReceiptService
        .query({
          page: this.page,
          size: this.itemsPerPage,
          sort: this.sort(),
        })
        .subscribe((res: HttpResponse<IOrderReceipt[]>) => this.paginateOrderReceipts(res.body, res.headers));
    } else {
      this.orderReceiptService
        .queryAsClient({
          page: this.page,
          size: this.itemsPerPage,
          sort: this.sort(),
        })
        .subscribe((res: HttpResponse<IOrderReceipt[]>) => this.paginateOrderReceipts(res.body, res.headers));
    }
  }

  reset(): void {
    this.page = 0;
    this.orderReceipts = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInOrderReceipts();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOrderReceipt): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOrderReceipts(): void {
    this.eventSubscriber = this.eventManager.subscribe('orderReceiptListModification', () => this.reset());
  }

  delete(orderReceipt: IOrderReceipt): void {
    const modalRef = this.modalService.open(OrderReceiptDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.orderReceipt = orderReceipt;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateOrderReceipts(data: IOrderReceipt[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.orderReceipts.push(data[i]);
      }
    }
  }
}
