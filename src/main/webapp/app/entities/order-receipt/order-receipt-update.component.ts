import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { IOrderReceipt, OrderReceipt } from 'app/shared/model/order-receipt.model';
import { OrderReceiptService } from './order-receipt.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { IManager } from 'app/shared/model/manager.model';
import { ManagerService } from 'app/entities/manager/manager.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';

type SelectableEntity = IClient | IManager;

@Component({
  selector: 'jhi-order-receipt-update',
  templateUrl: './order-receipt-update.component.html',
})
export class OrderReceiptUpdateComponent implements OnInit {
  isSaving = false;
  clients: IClient[] = [];
  managers: IManager[] = [];
  account: Account | null = null;
  authSubscription?: Subscription;
  editForm = this.fb.group({
    id: [],
    totalItemCount: [],
    totalOrderWeight: [],
    totalOrderVolume: [],
    client: [],
    manager: [],
  });

  constructor(
    protected orderReceiptService: OrderReceiptService,
    protected clientService: ClientService,
    protected accountService: AccountService,
    protected managerService: ManagerService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.activatedRoute.data.subscribe(({ orderReceipt }) => {
      this.updateForm(orderReceipt);

      this.clientService.query().subscribe((res: HttpResponse<IClient[]>) => (this.clients = res.body || []));

      this.managerService.query().subscribe((res: HttpResponse<IManager[]>) => (this.managers = res.body || []));
    });
  }

  updateForm(orderReceipt: IOrderReceipt): void {
    this.editForm.patchValue({
      id: orderReceipt.id,
      totalItemCount: orderReceipt.totalItemCount,
      totalOrderWeight: orderReceipt.totalOrderWeight,
      totalOrderVolume: orderReceipt.totalOrderVolume,
      manager: orderReceipt.manager,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orderReceipt = this.createFromForm();
    if (orderReceipt.id !== undefined) {
      this.subscribeToSaveResponse(this.orderReceiptService.update(orderReceipt));
    } else {
      this.subscribeToSaveResponse(this.orderReceiptService.create(orderReceipt));
    }
  }

  private createFromForm(): IOrderReceipt {
    return {
      ...new OrderReceipt(),
      id: this.editForm.get(['id'])!.value,
      totalItemCount: this.editForm.get(['totalItemCount'])!.value,
      totalOrderWeight: this.editForm.get(['totalOrderWeight'])!.value,
      totalOrderVolume: this.editForm.get(['totalOrderVolume'])!.value,
      client: this.editForm.get(['client'])!.value,
      manager: this.editForm.get(['manager'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderReceipt>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
