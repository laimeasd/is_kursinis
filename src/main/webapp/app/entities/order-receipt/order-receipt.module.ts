import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipSharedModule } from 'app/shared/shared.module';
import { OrderReceiptComponent } from './order-receipt.component';
import { OrderReceiptDetailComponent } from './order-receipt-detail.component';
import { OrderReceiptUpdateComponent } from './order-receipt-update.component';
import { OrderReceiptDeleteDialogComponent } from './order-receipt-delete-dialog.component';
import { orderReceiptRoute } from './order-receipt.route';

@NgModule({
  imports: [JhipSharedModule, RouterModule.forChild(orderReceiptRoute)],
  declarations: [OrderReceiptComponent, OrderReceiptDetailComponent, OrderReceiptUpdateComponent, OrderReceiptDeleteDialogComponent],
  entryComponents: [OrderReceiptDeleteDialogComponent],
})
export class JhipOrderReceiptModule {}
