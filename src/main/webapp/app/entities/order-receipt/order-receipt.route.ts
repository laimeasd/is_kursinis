import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOrderReceipt, OrderReceipt } from 'app/shared/model/order-receipt.model';
import { OrderReceiptService } from './order-receipt.service';
import { OrderReceiptComponent } from './order-receipt.component';
import { OrderReceiptDetailComponent } from './order-receipt-detail.component';
import { OrderReceiptUpdateComponent } from './order-receipt-update.component';

@Injectable({ providedIn: 'root' })
export class OrderReceiptResolve implements Resolve<IOrderReceipt> {
  constructor(private service: OrderReceiptService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrderReceipt> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((orderReceipt: HttpResponse<OrderReceipt>) => {
          if (orderReceipt.body) {
            return of(orderReceipt.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OrderReceipt());
  }
}

export const orderReceiptRoute: Routes = [
  {
    path: '',
    component: OrderReceiptComponent,
    data: {
      authorities: [Authority.USER, Authority.MANAGER],
      pageTitle: 'OrderReceipts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OrderReceiptDetailComponent,
    resolve: {
      orderReceipt: OrderReceiptResolve,
    },
    data: {
      authorities: [Authority.USER, Authority.MANAGER],
      pageTitle: 'OrderReceipts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OrderReceiptUpdateComponent,
    resolve: {
      orderReceipt: OrderReceiptResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'OrderReceipts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OrderReceiptUpdateComponent,
    resolve: {
      orderReceipt: OrderReceiptResolve,
    },
    data: {
      authorities: [Authority.USER, Authority.MANAGER],
      pageTitle: 'OrderReceipts',
    },
    canActivate: [UserRouteAccessService],
  },
];
