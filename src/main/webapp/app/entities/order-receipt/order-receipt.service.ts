import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrderReceipt } from 'app/shared/model/order-receipt.model';

type EntityResponseType = HttpResponse<IOrderReceipt>;
type EntityArrayResponseType = HttpResponse<IOrderReceipt[]>;

@Injectable({ providedIn: 'root' })
export class OrderReceiptService {
  public resourceUrl = SERVER_API_URL + 'api/order-receipts';

  constructor(protected http: HttpClient) {}

  create(orderReceipt: IOrderReceipt): Observable<EntityResponseType> {
    return this.http.post<IOrderReceipt>(`${this.resourceUrl}/asd`, orderReceipt, { observe: 'response' });
  }

  update(orderReceipt: IOrderReceipt): Observable<EntityResponseType> {
    return this.http.put<IOrderReceipt>(this.resourceUrl, orderReceipt, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOrderReceipt>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOrderReceipt[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
  queryAsClient(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOrderReceipt[]>(`${this.resourceUrl}/myorders`, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
