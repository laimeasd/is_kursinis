import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IItemList } from 'app/shared/model/item-list.model';
import { ItemListService } from './item-list.service';

@Component({
  templateUrl: './item-list-delete-dialog.component.html',
})
export class ItemListDeleteDialogComponent {
  itemList?: IItemList;

  constructor(protected itemListService: ItemListService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.itemListService.delete(id).subscribe(() => {
      this.eventManager.broadcast('itemListListModification');
      this.activeModal.close();
    });
  }
}
