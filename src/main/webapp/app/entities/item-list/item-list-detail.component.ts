import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IItemList } from 'app/shared/model/item-list.model';

@Component({
  selector: 'jhi-item-list-detail',
  templateUrl: './item-list-detail.component.html',
})
export class ItemListDetailComponent implements OnInit {
  itemList: IItemList | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ itemList }) => (this.itemList = itemList));
  }

  previousState(): void {
    window.history.back();
  }
}
