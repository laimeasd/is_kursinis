import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IItemList, ItemList } from 'app/shared/model/item-list.model';
import { ItemListService } from './item-list.service';
import { IOrderReceipt } from 'app/shared/model/order-receipt.model';
import { OrderReceiptService } from 'app/entities/order-receipt/order-receipt.service';

@Component({
  selector: 'jhi-item-list-update',
  templateUrl: './item-list-update.component.html',
})
export class ItemListUpdateComponent implements OnInit {
  isSaving = false;
  orderreceipts: IOrderReceipt[] = [];

  editForm = this.fb.group({
    id: [],
    itemCount: [],
    listPurpose: [],
    orderreceipt: [],
  });

  constructor(
    protected itemListService: ItemListService,
    protected orderReceiptService: OrderReceiptService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ itemList }) => {
      this.updateForm(itemList);

      this.orderReceiptService.query().subscribe((res: HttpResponse<IOrderReceipt[]>) => (this.orderreceipts = res.body || []));
    });
  }

  updateForm(itemList: IItemList): void {
    this.editForm.patchValue({
      id: itemList.id,
      itemCount: itemList.itemCount,
      listPurpose: itemList.listPurpose,
      orderreceipt: itemList.orderreceipt,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const itemList = this.createFromForm();
    if (itemList.id !== undefined) {
      this.subscribeToSaveResponse(this.itemListService.update(itemList));
    } else {
      this.subscribeToSaveResponse(this.itemListService.create(itemList));
    }
  }

  private createFromForm(): IItemList {
    return {
      ...new ItemList(),
      id: this.editForm.get(['id'])!.value,
      itemCount: this.editForm.get(['itemCount'])!.value,
      listPurpose: this.editForm.get(['listPurpose'])!.value,
      orderreceipt: this.editForm.get(['orderreceipt'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IItemList>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOrderReceipt): any {
    return item.id;
  }
}
