import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IItemList } from 'app/shared/model/item-list.model';

type EntityResponseType = HttpResponse<IItemList>;
type EntityArrayResponseType = HttpResponse<IItemList[]>;

@Injectable({ providedIn: 'root' })
export class ItemListService {
  public resourceUrl = SERVER_API_URL + 'api/item-lists';

  constructor(protected http: HttpClient) {}

  create(itemList: IItemList): Observable<EntityResponseType> {
    return this.http.post<IItemList>(this.resourceUrl, itemList, { observe: 'response' });
  }

  createforOrder(id: number, itemList: IItemList): Observable<HttpResponse<IItemList>> {
    return this.http.post<IItemList>(`${this.resourceUrl}/${id}/addForOrder`, itemList, { observe: 'response' });
  }

  update(itemList: IItemList): Observable<EntityResponseType> {
    return this.http.put<IItemList>(this.resourceUrl, itemList, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IItemList>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  findByOrder(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<IItemList[]>(`${this.resourceUrl}/byorder/${id}`, { observe: 'response' });
  }
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IItemList[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
  queryByOrder(orderid: number): Observable<EntityArrayResponseType> {
    return this.http.get<IItemList[]>(`${this.resourceUrl}/${orderid}`, { observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
