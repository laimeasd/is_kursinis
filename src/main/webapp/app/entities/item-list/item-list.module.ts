import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipSharedModule } from 'app/shared/shared.module';
import { ItemListComponent } from './item-list.component';
import { ItemListDetailComponent } from './item-list-detail.component';
import { ItemListUpdateComponent } from './item-list-update.component';
import { ItemListDeleteDialogComponent } from './item-list-delete-dialog.component';
import { itemListRoute } from './item-list.route';
import { orderListRoute } from '../order-list/order-list.route';

@NgModule({
  //imports: [JhipSharedModule, RouterModule.forChild(itemListRoute), RouterModule.forChild(orderListRoute)],
  imports: [JhipSharedModule, RouterModule.forChild(itemListRoute)],
  declarations: [ItemListComponent, ItemListDetailComponent, ItemListUpdateComponent, ItemListDeleteDialogComponent],
  entryComponents: [ItemListDeleteDialogComponent],
})
export class JhipItemListModule {}
