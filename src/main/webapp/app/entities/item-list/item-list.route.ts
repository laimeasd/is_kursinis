import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IItemList, ItemList } from 'app/shared/model/item-list.model';
import { ItemListService } from './item-list.service';
import { ItemListComponent } from './item-list.component';
import { ItemListDetailComponent } from './item-list-detail.component';
import { ItemListUpdateComponent } from './item-list-update.component';
import { ItemListByOrderComponent } from '../order-list/item-list-by-order.component';
import { OrderListResolve } from '../order-list/order-list.route';

@Injectable({ providedIn: 'root' })
export class ItemListResolve implements Resolve<IItemList> {
  constructor(private service: ItemListService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IItemList> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((itemList: HttpResponse<ItemList>) => {
          if (itemList.body) {
            return of(itemList.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ItemList());
  }
}

export const itemListRoute: Routes = [
  {
    path: '',
    component: ItemListComponent,
    data: {
      authorities: [Authority.USER],

      defaultSort: 'id,asc',
      pageTitle: 'ItemLists',
    },
    canActivate: [UserRouteAccessService],
  } /*,
  {
    path: 'byorder/:id',
    component: ItemListByOrderComponent,
    resolve:{
      itemListByOrder: OrderListResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ItemLists',
    }
  }*/,
  {
    path: ':id/view',
    component: ItemListDetailComponent,
    resolve: {
      itemList: ItemListResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ItemLists',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ItemListUpdateComponent,
    resolve: {
      itemList: ItemListResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ItemLists',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ItemListUpdateComponent,
    resolve: {
      itemList: ItemListResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ItemLists',
    },
    canActivate: [UserRouteAccessService],
  },
];
