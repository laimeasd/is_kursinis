import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipSharedModule } from 'app/shared/shared.module';
import { ItemComponent } from './item.component';
import { ItemDetailComponent } from './item-detail.component';
import { ItemUpdateComponent } from './item-update.component';
import { ItemDeleteDialogComponent } from './item-delete-dialog.component';
import { itemRoute } from './item.route';
import { itemsRoute } from '../list-item/list-item.route';

@NgModule({
  imports: [JhipSharedModule, RouterModule.forChild(itemRoute), RouterModule.forChild(itemsRoute)],
  declarations: [ItemComponent, ItemDetailComponent, ItemUpdateComponent, ItemDeleteDialogComponent],
  entryComponents: [ItemDeleteDialogComponent],
})
export class JhipItemModule {}
