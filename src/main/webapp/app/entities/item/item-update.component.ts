import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IItem, Item } from 'app/shared/model/item.model';
import { ItemService } from './item.service';
import { IItemList } from 'app/shared/model/item-list.model';
import { ItemListService } from 'app/entities/item-list/item-list.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { IWhoperative } from 'app/shared/model/whoperative.model';
import { WhoperativeService } from 'app/entities/whoperative/whoperative.service';

type SelectableEntity = IItemList | IClient | IWhoperative;

@Component({
  selector: 'jhi-item-update',
  templateUrl: './item-update.component.html',
})
export class ItemUpdateComponent implements OnInit {
  isSaving = false;
  itemlists: IItemList[] = [];
  clients: IClient[] = [];
  whoperatives: IWhoperative[] = [];

  editForm = this.fb.group({
    id: [],
    itemName: [],
    itemWeight: [],
    itemVolume: [],
    location: [],
    itemlist: [],
    client: [],
    whoperative: [],
  });

  constructor(
    protected itemService: ItemService,
    protected itemListService: ItemListService,
    protected clientService: ClientService,
    protected whoperativeService: WhoperativeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ item }) => {
      this.updateForm(item);

      this.itemListService.query().subscribe((res: HttpResponse<IItemList[]>) => (this.itemlists = res.body || []));

      this.clientService.query().subscribe((res: HttpResponse<IClient[]>) => (this.clients = res.body || []));

      this.whoperativeService.query().subscribe((res: HttpResponse<IWhoperative[]>) => (this.whoperatives = res.body || []));
    });
  }

  updateForm(item: IItem): void {
    this.editForm.patchValue({
      id: item.id,
      itemName: item.itemName,
      itemWeight: item.itemWeight,
      itemVolume: item.itemVolume,
      location: item.location,
      itemlist: item.itemlist,
      client: item.client,
      whoperative: item.whoperative,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const item = this.createFromForm();
    if (item.id !== undefined) {
      this.subscribeToSaveResponse(this.itemService.update(item));
    } else {
      this.subscribeToSaveResponse(this.itemService.create(item));
    }
  }

  private createFromForm(): IItem {
    return {
      ...new Item(),
      id: this.editForm.get(['id'])!.value,
      itemName: this.editForm.get(['itemName'])!.value,
      itemWeight: this.editForm.get(['itemWeight'])!.value,
      itemVolume: this.editForm.get(['itemVolume'])!.value,
      location: this.editForm.get(['location'])!.value,
      itemlist: this.editForm.get(['itemlist'])!.value,
      client: this.editForm.get(['client'])!.value,
      whoperative: this.editForm.get(['whoperative'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IItem>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
