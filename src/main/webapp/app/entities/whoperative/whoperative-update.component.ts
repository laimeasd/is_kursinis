import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IWhoperative, Whoperative } from 'app/shared/model/whoperative.model';
import { WhoperativeService } from './whoperative.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IOrderReceipt } from 'app/shared/model/order-receipt.model';
import { OrderReceiptService } from 'app/entities/order-receipt/order-receipt.service';

type SelectableEntity = IUser | IOrderReceipt;

@Component({
  selector: 'jhi-whoperative-update',
  templateUrl: './whoperative-update.component.html',
})
export class WhoperativeUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  orderreceipts: IOrderReceipt[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    surname: [],
    user: [],
    orderreceipts: [],
  });

  constructor(
    protected whoperativeService: WhoperativeService,
    protected userService: UserService,
    protected orderReceiptService: OrderReceiptService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ whoperative }) => {
      this.updateForm(whoperative);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.orderReceiptService.query().subscribe((res: HttpResponse<IOrderReceipt[]>) => (this.orderreceipts = res.body || []));
    });
  }

  updateForm(whoperative: IWhoperative): void {
    this.editForm.patchValue({
      id: whoperative.id,
      name: whoperative.name,
      surname: whoperative.surname,
      user: whoperative.user,
      orderreceipts: whoperative.orderreceipts,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const whoperative = this.createFromForm();
    if (whoperative.id !== undefined) {
      this.subscribeToSaveResponse(this.whoperativeService.update(whoperative));
    } else {
      this.subscribeToSaveResponse(this.whoperativeService.create(whoperative));
    }
  }

  private createFromForm(): IWhoperative {
    return {
      ...new Whoperative(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      surname: this.editForm.get(['surname'])!.value,
      user: this.editForm.get(['user'])!.value,
      orderreceipts: this.editForm.get(['orderreceipts'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWhoperative>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IOrderReceipt[], option: IOrderReceipt): IOrderReceipt {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
