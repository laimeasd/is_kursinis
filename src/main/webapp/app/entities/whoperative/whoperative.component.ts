import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWhoperative } from 'app/shared/model/whoperative.model';
import { WhoperativeService } from './whoperative.service';
import { WhoperativeDeleteDialogComponent } from './whoperative-delete-dialog.component';

@Component({
  selector: 'jhi-whoperative',
  templateUrl: './whoperative.component.html',
})
export class WhoperativeComponent implements OnInit, OnDestroy {
  whoperatives?: IWhoperative[];
  eventSubscriber?: Subscription;

  constructor(
    protected whoperativeService: WhoperativeService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.whoperativeService.query().subscribe((res: HttpResponse<IWhoperative[]>) => (this.whoperatives = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInWhoperatives();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IWhoperative): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInWhoperatives(): void {
    this.eventSubscriber = this.eventManager.subscribe('whoperativeListModification', () => this.loadAll());
  }

  delete(whoperative: IWhoperative): void {
    const modalRef = this.modalService.open(WhoperativeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.whoperative = whoperative;
  }
}
