import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipSharedModule } from 'app/shared/shared.module';
import { WhoperativeComponent } from './whoperative.component';
import { WhoperativeDetailComponent } from './whoperative-detail.component';
import { WhoperativeUpdateComponent } from './whoperative-update.component';
import { WhoperativeDeleteDialogComponent } from './whoperative-delete-dialog.component';
import { whoperativeRoute } from './whoperative.route';

@NgModule({
  imports: [JhipSharedModule, RouterModule.forChild(whoperativeRoute)],
  declarations: [WhoperativeComponent, WhoperativeDetailComponent, WhoperativeUpdateComponent, WhoperativeDeleteDialogComponent],
  entryComponents: [WhoperativeDeleteDialogComponent],
})
export class JhipWhoperativeModule {}
