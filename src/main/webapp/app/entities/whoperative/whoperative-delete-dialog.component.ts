import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWhoperative } from 'app/shared/model/whoperative.model';
import { WhoperativeService } from './whoperative.service';

@Component({
  templateUrl: './whoperative-delete-dialog.component.html',
})
export class WhoperativeDeleteDialogComponent {
  whoperative?: IWhoperative;

  constructor(
    protected whoperativeService: WhoperativeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.whoperativeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('whoperativeListModification');
      this.activeModal.close();
    });
  }
}
