import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWhoperative } from 'app/shared/model/whoperative.model';

@Component({
  selector: 'jhi-whoperative-detail',
  templateUrl: './whoperative-detail.component.html',
})
export class WhoperativeDetailComponent implements OnInit {
  whoperative: IWhoperative | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ whoperative }) => (this.whoperative = whoperative));
  }

  previousState(): void {
    window.history.back();
  }
}
