import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWhoperative } from 'app/shared/model/whoperative.model';

type EntityResponseType = HttpResponse<IWhoperative>;
type EntityArrayResponseType = HttpResponse<IWhoperative[]>;

@Injectable({ providedIn: 'root' })
export class WhoperativeService {
  public resourceUrl = SERVER_API_URL + 'api/whoperatives';

  constructor(protected http: HttpClient) {}

  create(whoperative: IWhoperative): Observable<EntityResponseType> {
    return this.http.post<IWhoperative>(this.resourceUrl, whoperative, { observe: 'response' });
  }

  update(whoperative: IWhoperative): Observable<EntityResponseType> {
    return this.http.put<IWhoperative>(this.resourceUrl, whoperative, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWhoperative>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWhoperative[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
