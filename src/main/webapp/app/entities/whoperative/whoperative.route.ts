import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IWhoperative, Whoperative } from 'app/shared/model/whoperative.model';
import { WhoperativeService } from './whoperative.service';
import { WhoperativeComponent } from './whoperative.component';
import { WhoperativeDetailComponent } from './whoperative-detail.component';
import { WhoperativeUpdateComponent } from './whoperative-update.component';

@Injectable({ providedIn: 'root' })
export class WhoperativeResolve implements Resolve<IWhoperative> {
  constructor(private service: WhoperativeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWhoperative> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((whoperative: HttpResponse<Whoperative>) => {
          if (whoperative.body) {
            return of(whoperative.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Whoperative());
  }
}

export const whoperativeRoute: Routes = [
  {
    path: '',
    component: WhoperativeComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Whoperatives',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WhoperativeDetailComponent,
    resolve: {
      whoperative: WhoperativeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Whoperatives',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WhoperativeUpdateComponent,
    resolve: {
      whoperative: WhoperativeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Whoperatives',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WhoperativeUpdateComponent,
    resolve: {
      whoperative: WhoperativeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Whoperatives',
    },
    canActivate: [UserRouteAccessService],
  },
];
