import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.JhipClientModule),
      },
      {
        path: 'manager',
        loadChildren: () => import('./manager/manager.module').then(m => m.JhipManagerModule),
      },
      {
        path: 'whoperative',
        loadChildren: () => import('./whoperative/whoperative.module').then(m => m.JhipWhoperativeModule),
      },
      {
        path: 'item',
        loadChildren: () => import('./item/item.module').then(m => m.JhipItemModule),
      },
      {
        path: 'item-list',
        loadChildren: () => import('./item-list/item-list.module').then(m => m.JhipItemListModule),
      },
      {
        path: 'order-receipt',
        loadChildren: () => import('./order-receipt/order-receipt.module').then(m => m.JhipOrderReceiptModule),
      },
      {
        path: 'order-list',
        loadChildren: () => import('./order-list/order-list.module').then(m => m.JhipOrderListModule),
      },
      {
        path: 'list-item',
        loadChildren: () => import('./list-item/list-item.module').then(m => m.JhipIListItemModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class JhipEntityModule {}
