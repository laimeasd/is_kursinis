import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IItemList, ItemList } from '../../shared/model/item-list.model';
import { ItemListService } from '../item-list/item-list.service';
import { IOrderReceipt } from '../../shared/model/order-receipt.model';
import { OrderReceiptService } from '../order-receipt/order-receipt.service';

@Component({
  selector: 'jhi-list-item-create',
  templateUrl: './list-item-create.component.html',
})
export class ListItemCreateComponent implements OnInit {
  id = 0;
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    itemCount: [],
    listPurpose: [],
    orderreceipt: [],
  });

  constructor(
    protected itemListService: ItemListService,
    protected orderReceiptService: OrderReceiptService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.data.orderId;
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const itemList = this.createFromForm();
    this.subscribeToSaveResponse(this.itemListService.createforOrder(this.id, itemList));
  }

  private createFromForm(): IItemList {
    return {
      ...new ItemList(),
      id: this.editForm.get(['id'])!.value,
      itemCount: this.editForm.get(['itemCount'])!.value,
      listPurpose: this.editForm.get(['listPurpose'])!.value,
      orderreceipt: this.editForm.get(['orderreceipt'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IItemList>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOrderReceipt): any {
    return item.id;
  }
}
