import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipSharedModule } from 'app/shared/shared.module';
import { orderListRoute } from '../order-list/order-list.route';
import { ItemListByOrderComponent } from './item-list-by-order.component';
import { ListItemCreateComponent } from './list-item-create.component';

@NgModule({
  imports: [JhipSharedModule, RouterModule.forChild(orderListRoute)],
  declarations: [ItemListByOrderComponent, ListItemCreateComponent],
})
export class JhipOrderListModule {}
