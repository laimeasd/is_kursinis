import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IItemList, ItemList } from 'app/shared/model/item-list.model';
import { ItemListService } from '../item-list/item-list.service';
import { ItemListByOrderComponent } from './item-list-by-order.component';
import { ListItemCreateComponent } from './list-item-create.component';

@Injectable({ providedIn: 'root' })
export class OrderListResolve implements Resolve<IItemList[]> {
  constructor(private service: ItemListService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IItemList[]> | Observable<never> {
    const id = route.params['id'];

    return this.service.findByOrder(id).pipe(
      flatMap((itemList: HttpResponse<ItemList[]>) => {
        if (itemList.body) {
          return of(itemList.body);
        } else {
          this.router.navigate(['404']);
          return EMPTY;
        }
      })
    );
  }
}

@Injectable({ providedIn: 'root' })
export class OrderIdResolve implements Resolve<number> {
  constructor(private service: ItemListService, private router: Router) {}
  resolve(route: ActivatedRouteSnapshot): Observable<number> | Observable<never> {
    return route.params['id'];
  }
}

export const orderListRoute: Routes = [
  {
    path: ':id',
    component: ItemListByOrderComponent,
    resolve: {
      itemListByOrder: OrderListResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ItemLists',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/add-new-list',
    component: ListItemCreateComponent,
    resolve: {
      orderId: OrderIdResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'New Item List',
    },
    canActivate: [UserRouteAccessService],
  },
];
