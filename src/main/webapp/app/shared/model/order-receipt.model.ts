import { IItemList } from 'app/shared/model/item-list.model';
import { IClient } from 'app/shared/model/client.model';
import { IManager } from 'app/shared/model/manager.model';
import { IWhoperative } from 'app/shared/model/whoperative.model';

export interface IOrderReceipt {
  id?: number;
  totalItemCount?: number;
  totalOrderWeight?: number;
  totalOrderVolume?: number;
  itemLists?: IItemList[];
  client?: IClient;
  manager?: IManager;
  whoperatives?: IWhoperative[];
}

export class OrderReceipt implements IOrderReceipt {
  constructor(
    public id?: number,
    public totalItemCount?: number,
    public totalOrderWeight?: number,
    public totalOrderVolume?: number,
    public itemLists?: IItemList[],
    public client?: IClient,
    public manager?: IManager,
    public whoperatives?: IWhoperative[]
  ) {}
}
