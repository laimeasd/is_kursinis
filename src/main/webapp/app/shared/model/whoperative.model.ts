import { IItem } from 'app/shared/model/item.model';
import { IUser } from 'app/core/user/user.model';
import { IOrderReceipt } from 'app/shared/model/order-receipt.model';

export interface IWhoperative {
  id?: number;
  name?: string;
  surname?: string;
  items?: IItem[];
  user?: IUser;
  orderreceipts?: IOrderReceipt[];
}

export class Whoperative implements IWhoperative {
  constructor(
    public id?: number,
    public name?: string,
    public surname?: string,
    public items?: IItem[],
    public user?: IUser,
    public orderreceipts?: IOrderReceipt[]
  ) {}
}
