import { IOrderReceipt } from 'app/shared/model/order-receipt.model';
import { IUser } from 'app/core/user/user.model';

export interface IManager {
  id?: number;
  name?: string;
  surname?: string;
  department?: string;
  orderReceipts?: IOrderReceipt[];
  user?: IUser;
}

export class Manager implements IManager {
  constructor(
    public id?: number,
    public name?: string,
    public surname?: string,
    public department?: string,
    public orderReceipts?: IOrderReceipt[],
    public user?: IUser
  ) {}
}
