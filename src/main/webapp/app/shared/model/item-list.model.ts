import { IItem } from 'app/shared/model/item.model';
import { IOrderReceipt } from 'app/shared/model/order-receipt.model';
import { ListPurpose } from 'app/shared/model/enumerations/list-purpose.model';

export interface IItemList {
  id?: number;
  itemCount?: number;
  listPurpose?: ListPurpose;
  items?: IItem[];
  orderreceipt?: IOrderReceipt;
}

export class ItemList implements IItemList {
  constructor(
    public id?: number,
    public itemCount?: number,
    public listPurpose?: ListPurpose,
    public items?: IItem[],
    public orderreceipt?: IOrderReceipt
  ) {}
}
