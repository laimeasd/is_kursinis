import { IItemList } from 'app/shared/model/item-list.model';
import { IClient } from 'app/shared/model/client.model';
import { IWhoperative } from 'app/shared/model/whoperative.model';

export interface IItem {
  id?: number;
  itemName?: string;
  itemWeight?: number;
  itemVolume?: number;
  location?: string;
  itemlist?: IItemList;
  client?: IClient;
  whoperative?: IWhoperative;
}

export class Item implements IItem {
  constructor(
    public id?: number,
    public itemName?: string,
    public itemWeight?: number,
    public itemVolume?: number,
    public location?: string,
    public itemlist?: IItemList,
    public client?: IClient,
    public whoperative?: IWhoperative
  ) {}
}
