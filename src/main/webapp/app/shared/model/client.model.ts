import { IOrderReceipt } from 'app/shared/model/order-receipt.model';
import { IItem } from 'app/shared/model/item.model';
import { IUser } from 'app/core/user/user.model';

export interface IClient {
  id?: number;
  name?: string;
  surname?: string;
  address?: string;
  orderReceipts?: IOrderReceipt[];
  items?: IItem[];
  user?: IUser;
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public name?: string,
    public surname?: string,
    public address?: string,
    public orderReceipts?: IOrderReceipt[],
    public items?: IItem[],
    public user?: IUser
  ) {}
}
